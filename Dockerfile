FROM nginx:latest
COPY start-nginx.sh /start-nginx.sh
COPY nginx/default.conf.template /etc/nginx/conf.d/default.conf.template
RUN chmod +x /start-nginx.sh
CMD ["/start-nginx.sh"]
